import sys
import math

__author__ = 'Joe'

import time
from Scripts.perlin import SimplexNoise, BaseNoise
from Scripts.spawn import instantiate

world_colors = {"ocean_deep": (0, 0, 128),
                "ocean": (64, 64, 196),
                "ocean_shallow": (128, 128, 255),
                "ocean_shoal": (200, 200, 255),
                "beach": (255, 255, 0),
                "mountain": (75, 75, 75),
                "snow": (245, 245, 245),
                "desert": (255, 255, 128),
                "swamp": (0, 128, 64),
                "ice": (128, 255, 255),
                "rainforest": (64, 255, 64),
                "bushland": (240, 230, 180),
                "rockland": (128, 128, 128),
                "plains_dirt": (225, 185, 135),
                "plains": (180, 225, 110),
                "plains_flowers": (30, 210, 20),
                "forest": (20, 120, 10),
                "forest_dense": (10, 80, 5),
                "-": (0, 0, 0)}


class Chunk:
    SIZE = 4

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.tile_data = []


class Tile:
    SWIM_TILES = ["ocean_deep", "ocean", "ocean_shallow", "ocean_deep", "swamp"]
    WALKABLE_OBJECTS = ["flower_pink", "flower_red", "flower_blue", "flower_yellow"]

    def __init__(self, tile_type, obj=""):
        self.type_long = tile_type.lower()
        self.type = tile_type[0]
        self.obj = obj
        self.swim = tile_type in Tile.SWIM_TILES


class World:
    def __init__(self, seed=12345):
        self.noise_temp = SimplexNoise()
        self.noise_wet = SimplexNoise()
        self.noise_height = SimplexNoise()
        self.noise_obj = SimplexNoise()
        self.seed = seed
        self.set_seed(seed)
        self.zoom_level = 64
        self.chunks = {}
        self.loaded_chunks = {}
        self.create_anim_tiles()

    def create_anim_tiles(self):
        for t in ["ocean_shoal", "ocean", "ocean_shallow", "ocean_deep", "swamp"]:
            ob = instantiate("tile_" + t)
            ob["anim"] = True
            ob.worldPosition = [0, 0, -10]

    def set_seed(self, seed):
        self.noise_temp.randomize(128, seed)
        self.noise_wet.randomize(128, seed*2)
        self.noise_obj.randomize(128, seed*3)
        self.noise_height.randomize(128, seed*4)

    def create_tile(self, x, y):
        xnoise = x
        ynoise = y
        n = self.zoom_level

        t1 = self.noise_temp.noise2(xnoise/(16.0*n), ynoise/(16.0*n))
        t2 = self.noise_temp.noise2(xnoise/(4.0*n), ynoise/(4.0*n))

        w1 = self.noise_wet.noise2(xnoise/(4.0*n), ynoise/(4.0*n))
        w2 = self.noise_wet.noise2(xnoise/(2.0*n), ynoise/(2.0*n))

        h1 = self.noise_height.noise2(xnoise/(8.0*n), ynoise/(8.0*n))
        h2 = self.noise_height.noise2(xnoise/(4.0*n), ynoise/(4.0*n))
        h3 = self.noise_height.noise2(xnoise/(2.0*n), ynoise/(2.0*n))
        h4 = self.noise_height.noise2(xnoise/(1.0*n), ynoise/(1.0*n))

        o1 = self.noise_obj.noise2(x*16, y*16)

        height = 0.5 + ((h1+(h2/2)+(h3/4)+(h4/4)) / 4)
        temperature = 0.5 + ((t1 + (t2/2)) / 3)
        wet = 0.5 + ((w1+(w2/2)) / 3)
        obj = 0.5 + (o1 / 2)

        if height < 0.25:
            tile = Tile("ocean_deep")
        elif height < 0.35:
            tile = Tile("ocean")
        elif height < 0.45:
            tile = Tile("ocean_shallow")
        elif height < 0.5:
            tile = Tile("ocean_shoal")
        elif height < 0.55:
            tile = Tile("beach")
        elif height > 0.8:
            tile = Tile("snow")
        elif height > 0.75:
            tile = Tile("mountain")
            if obj > 0.9 or obj < 0.1:
                    tile.obj = "rock"
        elif wet < 0.3:
            if temperature < 0.15:
                tile = Tile("rockland")
                if obj > 0.9 or obj < 0.1:
                    tile.obj = "rock"
            elif temperature < 0.5:
                tile = Tile("bushland")
            else:
                tile = Tile("desert")
                if obj > 0.9 or obj < 0.1:
                    tile.obj = "cactus"
        elif wet < 0.6:
            if temperature < 0.2:
                tile = Tile("plains_dirt")
            elif temperature < 0.4:
                tile = Tile("plains")
            elif temperature < 0.6:
                tile = Tile("plains_flowers")
                if 0.75 > obj > 0.6:
                    tile.obj = "flower_pink"
                elif obj > 0.85:
                    tile.obj = "flower_blue"
                elif 0.3 < obj < 0.45:
                    tile.obj = "flower_red"
                elif obj < 0.15:
                    tile.obj = "flower_yellow"
            elif temperature < 0.8:
                tile = Tile("forest")
                if obj > 0.8 or obj < 0.2:
                    tile.obj = "tree"
            else:
                tile = Tile("forest_dense")
                if obj > 0.7 or obj < 0.3:
                    tile.obj = "tree"
        else:
            if temperature < 0.15:
                tile = Tile("ice")
            elif temperature < 0.5:
                tile = Tile("swamp")
            else:
                tile = Tile("rainforest")

        return tile

    def spawn_chunk(self, xypos):
        x_pos, y_pos = xypos
        ob_empty = instantiate("chunk_parent")

        chunk_data = self.get_chunk(x_pos, y_pos)

        for y in range(Chunk.SIZE):
            for x in range(Chunk.SIZE):
                tile_data = chunk_data.tile_data[(y*Chunk.SIZE)+x]
                color = world_colors[tile_data.type_long]
                r = color[0]/255.0
                g = color[1]/255.0
                b = color[2]/255.0

                ob_tile = instantiate("tile_" + tile_data.type_long)
                ob_tile.worldPosition = [x, y, 0]

                if tile_data.obj != "":
                    ob_object = instantiate("ob_" + tile_data.obj, ob_tile.worldPosition)
                    ob_object.setParent(ob_empty)

                ob_tile.setParent(ob_empty)


        ob_empty.worldPosition = [x_pos * Chunk.SIZE, y_pos * Chunk.SIZE, 0]

        self.loaded_chunks[(x_pos, y_pos)] = ob_empty


    def get_chunk(self, x_pos, y_pos):
        if (x_pos, y_pos) not in self.chunks:
            self.create_chunk(x_pos, y_pos)

        return self.chunks[(x_pos, y_pos)]

    def create_chunk(self, x_pos, y_pos):
        new_chunk = Chunk(x_pos, y_pos)

        for y in range(Chunk.SIZE):
            for x in range(Chunk.SIZE):
                new_chunk.tile_data.append(self.create_tile((x_pos*Chunk.SIZE)+x, (y_pos*Chunk.SIZE)+y))

        self.chunks[(x_pos, y_pos)] = new_chunk

    def get_chunk_of_position(self, x, y):
        return math.floor(x / Chunk.SIZE), math.floor(y / Chunk.SIZE)

    def check_active_bounds(self, player_pos):
        current_chunk_id = self.get_chunk_of_position(player_pos[0], player_pos[1])

        chunks_to_keep = []

        for x in range(-3, 4):
            for y in range(-2, 3):
                chunk_id = (current_chunk_id[0] + x, current_chunk_id[1] + y)
                if chunk_id not in self.loaded_chunks:
                    self.spawn_chunk(chunk_id)
                chunks_to_keep.append(chunk_id)

        chunks_to_delete = []

        for c in self.loaded_chunks.keys():
            if c not in chunks_to_keep:
                chunks_to_delete.append(c)

        for c in chunks_to_delete:
            self.loaded_chunks[c].endObject()
            del self.loaded_chunks[c]

    def get_tile_at(self, world_pos):
        x, y, z = world_pos
        x = math.floor(x+0.5)
        y = math.floor(y+0.5)
        chunk_id = self.get_chunk_of_position(x, y)
        c_x = int(x) % Chunk.SIZE
        c_y = int(y) % Chunk.SIZE

        if chunk_id not in self.chunks:
            return Tile("-")

        chunk = self.chunks[chunk_id]

        return chunk.tile_data[int((c_y*Chunk.SIZE)+c_x)]

    def pos_walkable(self, worldpos):
        tile = self.get_tile_at(worldpos)

        if tile.obj is "":
            return True

        if tile.obj in Tile.WALKABLE_OBJECTS:
            return True

        return False