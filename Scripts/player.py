from bge import logic
from mathutils import Vector

scene = logic.getCurrentScene()
OB = scene.objects
this = OB['MAIN']


class Player:
    TOOLS = ["shovel", "pickaxe", "axe", "hoe", "hammer"]

    def __init__(self):
        self.obj = scene.addObject("Player", this)
        self.cursor = scene.addObject("player_cursor", this)
        self.cursor_pos = (0, 0)
        self.rot_int = 0
        self.can_move = 1
        self.move_speed = 0.1
        self.world = None
        self.facing_object = ""
        self.set_tool("none")
        self.tool_idx = 0


    def active(self):
        self.test_floor()
        self.move()
        self.tool_controls()
        # self.cursor_move()
        self.camera_follow()
        self.set_debug_text()

    def move(self):
        keys = logic.keyboard.events

        dir_vec = Vector((0, 0, 0))

        if keys[146] == 2:  # UP
            dir_vec += Vector((0, 1, 0))
        if keys[144] == 2:  # DOWN
            dir_vec += Vector((0, -1, 0))
        if keys[143] == 2:  # LEFT
            dir_vec += Vector((-1, 0, 0))
        if keys[145] == 2:  # RIGHT
            dir_vec += Vector((1, 0, 0))

        self.obj['run'] = False

        if dir_vec != Vector((0, 0, 0)):
            self.obj['run'] = True
            dir_vec.normalize()

            target_location = self.obj.worldPosition + (dir_vec * 0.2)
            self.facing_object = self.world.get_tile_at(target_location).obj
            target_passable = self.world.pos_walkable(target_location)

            move_spd = self.move_speed
            if not target_passable:
                move_spd = 0

            if self.obj["swim"]:
                move_spd *= 0.6

            self.obj.worldPosition += dir_vec * move_spd
            self.obj.alignAxisToVect(dir_vec, 1, 1)
            self.obj.alignAxisToVect(Vector((0, 0, 1)), 2, 1)

    def tool_controls(self):
        keys = logic.keyboard.events
        if keys[97] == 1:
            self.tool_idx += 1
            if self.tool_idx >= len(Player.TOOLS):
                self.tool_idx = 0

            current_tool = Player.TOOLS[self.tool_idx]

            self.obj["change_tool"] = 0.5
            self.set_tool(current_tool)



    def set_tool(self, tool_id):
        tool_parent = self.obj.children[0]

        child_count = len(tool_parent.children)

        for i in range(child_count):
            tool_parent.children[i].endObject()

        tool_ob = scene.addObject("tool_" + tool_id, tool_parent)
        tool_ob.setParent(tool_parent)

    def set_debug_text(self):
        o = OB["DEBUGTEXT_player"]
        wp = self.obj.worldPosition
        chunk = self.world.get_chunk_of_position(wp[0], wp[1])
        tile_below = self.world.get_tile_at(wp)


        txt = "world_pos = [{}, {}]\n".format(float(int(wp[0]*10))/10, float(int(wp[1]*10))/10)
        txt += "chunk_id = [{}, {}]\n".format(*chunk)
        txt += "tile_below = {}\n".format(tile_below.type_long)
        txt += "facing object = {}".format(self.facing_object)
        o['Text'] = txt


    def test_floor(self):
        if self.world is None:
            return

        floor_tile = self.world.get_tile_at(self.obj.worldPosition)
        self.obj["swim"] = floor_tile.swim

    def test_hit(self):
        pass

    def cursor_move(self):
        self.cursor.worldPosition = (0, 0, -10)

        frompos = self.obj.worldPosition + self.obj.getAxisVect((0, 1, 0)) + Vector((0, 0, 1))
        topos = self.obj.worldPosition + self.obj.getAxisVect((0, 1, 0)) - Vector((0, 0, 1))

        ray = self.obj.rayCast(frompos, topos, 2, "tile", 1, 1, 1)
        if ray[0]:
            self.cursor.worldPosition = ray[0].worldPosition
            self.cursor_pos = (int(ray[0].worldPosition[0] + 7.5), int(ray[0].worldPosition[1] + 7.5))

        frompos = self.obj.worldPosition + (self.obj.getAxisVect((0, 1, 0)) * 0.2) + Vector((0, 0, 1))
        topos = self.obj.worldPosition + (self.obj.getAxisVect((0, 1, 0)) * 0.2) - Vector((0, 0, 1))
        ray = self.obj.rayCast(frompos, topos, 2, "tile", 1, 1, 1)
        if ray[0]:
            if "tile" in ray[0]:
                if ray[0]["tile"] in ["grass", "ice", "sand", "dirt", "rock", "water"]:
                    self.can_move = 1
                else:
                    self.can_move = 0

        frompos = self.obj.worldPosition + Vector((0, 0, 1))
        topos = self.obj.worldPosition - Vector((0, 0, 1))
        ray = self.obj.rayCast(frompos, topos, 2, "tile", 1, 1, 1)
        if ray[0]:
            if "tile" in ray[0]:
                if ray[0]["tile"] == "water":
                    self.obj["swim"] = True
                    self.cursor.setVisible(False, False)
                else:
                    self.obj["swim"] = False
                    self.cursor.setVisible(True, False)

    def camera_follow(self):
        OB["Camera"].worldPosition = self.obj.worldPosition + Vector((0, -13.5, 15))
