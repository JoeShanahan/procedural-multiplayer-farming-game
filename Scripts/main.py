from bge import logic, render

scene = logic.getCurrentScene()
OB = scene.objects
this = OB['MAIN']

from Scripts.player import Player
from Scripts.world import World

from mathutils import Vector


class Game:
    def __init__(self):
        self.world = World(7473850175)
        self.player = Player()
        self.player.world = self.world

    def Update(self):
        self.player.active()
        self.world.check_active_bounds(self.player.obj.worldPosition)

game = Game()

def update():
    game.Update()