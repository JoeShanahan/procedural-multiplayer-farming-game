__author__ = 'Joe'

from bge import logic, render

scene = logic.getCurrentScene()
OB = scene.objects
this = OB['MAIN']

import globals as g
import random
import time
import socket

from mathutils import Vector


dir_int = [Vector((0, 1, 0)), Vector((1, 1, 0)),
           Vector((1, 0, 0)), Vector((1, -1, 0)),
           Vector((0, -1, 0)), Vector((-1, -1, 0)),
           Vector((-1, 0, 0)), Vector((-1, 1, 0))]

class Network:
    def __init__(self, server_ip=socket.gethostbyname(socket.gethostname()), server_port=9999):
        server_ip = "192.168.1.124"
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False)

        self.serv_addr = (server_ip, server_port)
        self.users = {}

    def send_to_server(self, data):
        self.socket.sendto(data.encode(), self.serv_addr)

    def set_pos(self, pos_data):
        player_id = pos_data[1]
        player_pos = pos_data[2].split(",")
        player_rot = pos_data[3]

        self.users[player_id].obj.worldPosition = float(player_pos[0]), float(player_pos[1]), 0
        rot_vec = dir_int[int(player_rot)]
        self.users[player_id].obj.alignAxisToVect(rot_vec, 1, 1)
        self.users[player_id].obj.alignAxisToVect((0, 0, 1), 2, 1)

        self.users[player_id].test_moving()

    def recv_from_server(self):
        while True:
            try:
                data, addr = self.socket.recvfrom(1024)
                data = data.decode()

                data = data.split(";")

                if data[0] == "P":
                    if data[1] not in self.users:
                        self.users[data[1]] = NetPlayer()

                    self.set_pos(data)
                    if g.transition == False:
                        self.users[data[1]].obj.setVisible(True, True)

                if data[0] == "p":
                    if data[1] in self.users:
                        self.users[data[1]].obj.setVisible(False, True)

                if data[0] == "M":
                    # TODO have rejection of map data (including blocked path)
                    g.map_data = data[1]
                    print(data[1])
                    g.waiting_for_server = False

                    display_map_data()
                    g.new_grid.worldPosition[0] -= g.transition_dir[0] * 16
                    g.new_grid.worldPosition[1] -= g.transition_dir[1] * 16

                    g.player.obj.setParent(g.new_grid)
                    g.transition = True

                if data[0] == "O":
                    display_map_objects(data[1])

                if data[0] == "t":
                    print(data)
                    for obj in g.current_grid.children:
                        if 'x' in obj:
                            if obj['x'] == int(data[2]):
                                if obj['y'] == int(data[3]):
                                    obj.endObject()
                                    obj = scene.addObject(get_color(data[1]), OB['MAIN'])
                                    obj['x'] = int(data[2])
                                    obj['y'] = int(data[3])
                                    obj.worldPosition = [obj['x'] - 7.5, (15 - obj['y']) - 7.5, 0]
                                    obj.setParent(g.current_grid)

                if data[0] == "b":
                    bullet = scene.addObject("bullet", this)
                    bullet.worldPosition = float(data[1]), float(data[2]), 0.5
                    bullet.alignAxisToVect(dir_int[int(data[3])], 1, 1)

            except socket.error:
                break

class NetPlayer:
    def __init__(self):
        self.obj = scene.addObject("Player", this)

        self.pos = (0, 0, 0)
        self.moving = False

    def test_moving(self):
        wp = [round(self.obj.worldPosition[0], 2), round(self.obj.worldPosition[1], 2)]

        if self.pos == wp:
            self.moving = False
        else:
            self.pos = wp
            self.moving = True

        self.obj["run"] = self.moving

def get_color(material):
    if material == "g":  # grass
        return "tile_grass"
    if material == "w":  # water
        return "tile_water"
    if material == "d":  # dirt
        return "tile_dirt"
    if material == "i":  # ice
        return "tile_ice"
    if material == "s":  # sand
        return "tile_dirt"
    if material == "c":  # sand
        return "tile_crate"
    if material == "r":  # sand
        return "tile_rock"

    return "Plane"

def display_map_data():
    ob_parent = scene.addObject("grid_parent", this)

    for y in range(16):
        for x in range(16):
            ypos = 15-y
            material = g.map_data[x + (16 * y)]

            ob = scene.addObject(get_color(material), this)
            ob.worldPosition = (x - 7.5, ypos - 7.5, 0)
            ob['x'] = x
            ob['y'] = y
            ob.setParent(ob_parent)

            ob = scene.addObject("Text", this)
            ob.worldPosition = (x - 7.75, ypos - 7.5, 0.25)
            ob['Text'] = "{},{}".format(x, y)
            ob.setParent(ob_parent)

    g.new_grid = ob_parent

def display_map_objects(all_obj_data):
    object_dict = {"T": "tree",
                   "C": "crate"}

    num_items = int(len(all_obj_data)/3)

    for i in range(num_items):
        obj_data = all_obj_data[3*i:3*(i+1)]
        if len(obj_data) != 3:
            continue
        obj_id = obj_data[0]
        y = int(obj_data[1], 16)
        x = int(obj_data[2], 16)

        if obj_id not in object_dict:
            continue

        x += g.new_grid.worldPosition[0]
        y += g.new_grid.worldPosition[1]

        #obj_name = object_dict[obj_id]
        #ob = scene.addObject("ob_{}".format(obj_name), this)
        #ob.worldPosition = (x - 7.5, y - 7.5, 0)
        #ob.setParent(g.new_grid)