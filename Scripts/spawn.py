__author__ = 'Joe'

from bge import logic

scene = logic.getCurrentScene()
this = logic.getCurrentController().owner


def instantiate(obj, position=None):
    try:
        new_ob = scene.addObject(obj, this)
    except Exception as ex:
        new_ob = scene.addObject("ERROR", this)
        print(ex)

    if position:
        new_ob.worldPosition = position

    return new_ob