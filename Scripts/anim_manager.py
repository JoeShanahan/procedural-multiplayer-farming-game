from bge import logic

scene = logic.getCurrentScene()

def player():
    cont = logic.getCurrentController()
    this = cont.owner
    action = cont.actuators['Action']
    
    cont.activate(action)
    
    if(this['swim']):
        action.action = "player_swim"
        action.frameEnd = 20
    elif(this['run']):
        action.action = "player_run"
        action.frameEnd = 10
    elif this["change_tool"] > 0:
        action.action = "player_change_tool"
        action.frameEnd = 10
        this["change_tool"] -= 1.0/60.0
    else:
        action.action = "player_idle"
        action.frameEnd = 10
