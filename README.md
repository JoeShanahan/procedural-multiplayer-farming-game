Copyright Joe Shanahan (2015)

Textures from Kenney Game Assets (http://kenney.itch.io/kenney-donation)

---

Software used:
* Blender (2.76)
* GIMP (2.8.8)
* PyCharm (4.0.6)

---

You're free to use this project for whatever you want, just as long as you don't
try and claim you made it.